# Desafio South System
### Teste manual
- Utilizando http client (postman, insomnia)
- Endereço: https://south-system.herokuapp.com
- Login manager:
  - manager
  - 123123
- Login user (para teste de autorização no CRUD):
  - user
  - 123

### Teste em linha de comando
- Requisitos
  - Docker >= v18
  - Docker compose >= v1.18
  *ou*
  - Node >= v13
  - Mongodb v4
  - Npm >= v6
- Usar .env de exemplo
- $ docker-compose up
- $ npm test

#### .env necessários
- .env (ajustar se não for usado com docker)
```
NODE_ENV=development
API_HOST=localhost
API_PORT=3030
MONGODB_URL=mongodb://mongo/online-api
APP_PRIVATE_KEY=-----BEGIN PRIVATE KEY-----\nMC4CAQAwBQYDK2VwBCIEIE/8nLWYgZpEObhL8pz9GhjYrqy5NO6q0iKU6iEFgtCm\n-----END PRIVATE KEY-----\n
```
- .env.test
```
NODE_ENV=development
API_HOST=localhost
API_PORT=3030
MONGODB_URL=mongodb://localhost/online-api
APP_PRIVATE_KEY=-----BEGIN PRIVATE KEY-----\nMC4CAQAwBQYDK2VwBCIEIE/8nLWYgZpEObhL8pz9GhjYrqy5NO6q0iKU6iEFgtCm\n-----END PRIVATE KEY-----\n
```
