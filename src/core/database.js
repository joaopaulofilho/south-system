const mongoose = require('mongoose')

if (!process.env.MONGODB_URL) {
  throw new Error('Database\'s uri not provided')
}

const mongo = async (app) => {
  await mongoose.connect(
    process.env.MONGODB_URL,
    {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    }
  ).catch(err => {
    // logger.error(err);
    console.error(err)
    process.exit(1)
  })

  mongoose.Promise = global.Promise

  mongoose.set('debug', process.env.NODE_ENV === 'development')

  if (app) {
    app.set('mongooseClient', mongoose)
  }

  return mongoose
}

module.exports = mongo