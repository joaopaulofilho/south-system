const Joi = require('joi')
const validate = require('express-joi-validation').createValidator({ passError: true })
const moment = require('moment')
const { Types: { ObjectId } } = require('mongoose')
const { ValidationError } = require('../errors')

const isMongoId = (value) => {
  const isValid = ObjectId.isValid(value)
  if (!isValid) {
    throw new ValidationError('id is not valid')
  }

  return value
}

const isDate = (value) => {
  if (!moment(value).isValid()) {
    throw new ValidationError('date is not valid')
  }

  return value
}

module.exports = {
  Joi,
  validate,
  isMongoId,
  isDate,
}
