const createApp = require('./app')

createApp()
  .then(app => {
    // const socket = require('socket.io')
    const port = process.env.PORT || process.env.API_PORT || 3000

    const server = app.listen(port)
    // socket.listen(server)

    server.on('listening', () =>
      console.log('Subscription app started on http://localhost:%d', port)
    )

    return {
      app,
      server,
    }
  })
