// See http://mongoosejs.com/docs/models.html
const mongoose = require('mongoose')

const { Schema } = mongoose
const instance = new Schema({

  name: { type: String, required: true },
  price: { type: Number, required: true },
  description: String,

}, {
  timestamps: true
})

const model = mongoose.model('Products', instance)

module.exports = model
