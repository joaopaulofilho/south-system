const { pick, each, get } = require('lodash')
const { NotFoundError } = require('../../errors')
const { Joi, validate, isMongoId } = require('../../core/validation')
const { authorization } = require('../../middlewares/auth')
const pagination = require('../../middlewares/pagination')
const Products = require('./model')

function checkMongoId(id, next) {
  try {
    isMongoId(id)
  } catch (e) {
    next(new NotFoundError())
  }
}

module.exports = function (app) {
  // find & get list
  app.get(
    '/products',

    validate.query(Joi.object({
      page: Joi.number(),
      search: Joi.string(),
    })),

    async (req, res, next) => {
      const limit = 20
      const page = get(req, 'query.page', 1)

      const query = {}
      if (req.query.search) {
        query.$or = []
        query.$or.push({ name: { $regex: req.query.search }})
        query.$or.push({ description: { $regex: req.query.search }})
      }

      const [products, total] = await Promise.all([
        Products
          .find(query)
          .skip(limit * (page -1)) // not scalable
          .limit(limit),
        Products
          .countDocuments(query),
      ]).catch(next)

      req.result = {
        limit,
        data: products,
        total,
      }

      next()
    },

    pagination(),
  )

  // get
  app.get(
    '/products/:id',

    async (req, res, next) => {
      checkMongoId(req.params.id, next)

      const product = await Products
        .findById(req.params.id)
        .catch(next)

      if (!product) {
        return next(new NotFoundError())
      }

      res.status(200).json(product)
    }
  )
  // create
  app.post(
    '/products',

    authorization(['managers']),

    validate.body(Joi.object({
      name: Joi.string().required(),
      price: Joi.number().required(),
      description: Joi.string(),
    })),

    async (req, res, next) => {
      const created = await Products
        .create(pick(req.body, ['name', 'price', 'description']))
        .catch(next)

      res.status(201).json(created)
    },
  )

  // update
  app.patch(
    '/products/:id',

    authorization(['managers']),

    validate.body(Joi.object({
      name: Joi.string(),
      price: Joi.number(),
      description: Joi.string(),
    })),

    async (req, res, next) => {
      checkMongoId(req.params.id, next)

      const product = await Products.findById(req.params.id).catch(next)

      if (!product) {
        return next(new NotFoundError())
      }

      each(pick(req.body, ['name', 'price', 'description']), (value, field) => {
        product[field] = value
      })

      await product.save().catch(next)

      res.status(200).json(product)
    },
  )

  // remove
  app.delete(
    '/products/:id',

    authorization(['managers']),

    async (req, res, next) => {
      checkMongoId(req.params.id, next)

      const _id = req.params.id

      const product = await Products.findById(_id).catch(next)

      if (!product) {
        return next(new NotFoundError())
      }

      await Products.deleteOne({ _id }).catch(next)

      res.status(200).json(product)
    },
  )
}
