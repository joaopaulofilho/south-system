const request = require('supertest')
const { generateToken } = require('../../../middlewares/auth')
const jestHack = require('../../../../test/helpers/jestHack')
const getApp = require('../../../../test/helpers/get-app')

jest.mock('../../managers/model', () => ({
  findById: () => ({ id: 1, name: 'João Paulo' }),
}))

jest.mock('../../products/model', () => ({
  create: async data => data,
  findById: async () => ({ id: 1, name: 'Producto MOCK', price: 200 }),
  deleteOne: async () => null,
}))

describe('Products CRUD', function () {
  let app, server, managerToken, userToken

  beforeAll(async function () {
    const { app: App, server: Server } = await getApp()
    app = App
    server = Server

    const { accessToken: mt } = await generateToken(1, 'managers')
    managerToken = { 'Authorization': `Bearer ${mt}` }

    const { accessToken: ut } = await generateToken(1, 'users')
    userToken = { 'Authorization': `Bearer ${ut}` }
  })

  afterAll(async function () {
    const mongo = app.get('mongooseClient')
    await mongo.close()
    await server.close()
  })

  describe('Create', function () {
    afterAll(jestHack)

    it('Should allow manager to create product', async function () {
      await request(app)
        .post('/products')
        .set(managerToken)
        .send({
          name: 'João Paulo',
          price: 20,
        })
        .expect(201)
    })
  })

  describe('Delete', function () {
    afterAll(jestHack)

    it('Should not allow user to delete product', async function () {
      await request(app)
        .delete('/products/5f42d65343sa6908cf900daa')
        .set(userToken)
        .expect(401)
    })
  })
})
