// See http://mongoosejs.com/docs/models.html
const mongoose = require('mongoose')

const { Schema } = mongoose
const instance = new Schema({

  name: { type: String, required: true },
  login: { type: String, lowercase: true, unique: true, index: true },
  password: { type: String, required: true },

}, {
  timestamps: true
})

const model = mongoose.model('Users', instance)

module.exports = model