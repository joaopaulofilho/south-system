const bcrypt = require('bcryptjs')
const { omit } = require('lodash')
const { authentication } = require('../../middlewares/auth')
const { Joi, validate } = require('../../core/validation')
const Managers = require('./model')

module.exports = function (app) {
  app.post(
    '/managers',

    validate.body(Joi.object({
      name: Joi.string().required(),
      login: Joi.string().required(),
      password: Joi.string().required(),
    })),

    async (req, res, next) => {
      const { name, login, password: rawPassword } = req.body
      const password = await bcrypt.hash(rawPassword, 10).catch(next)
      const created = await Managers
        .create({ name, login, password })
        .catch(next)
      res.status(201).json(omit(created.toJSON(), ['password']))
    },
  )

  app.post('/managers/login', authentication())
}
