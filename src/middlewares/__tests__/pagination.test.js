const pagination = require('../pagination')

describe('Pagination middleware', function () {
  const req = {}
  const res = ({ status: () => ({ json: data => data }) })
  const next = jest.fn()

  it('Should not paginate', function () {
    pagination()(req, res, next)
    expect(next).toBeCalled()
  })

  it('Should calculate total pages', function () {
    const result = pagination()({
      query: { page: 1 },
      result: { limit: 2, total: 3, data: ['a', 'b', 'c'] },
    }, res, next)

    expect(result.pages).toBe(2)
  })
})