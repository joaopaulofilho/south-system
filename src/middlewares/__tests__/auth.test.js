const { UnauthorizedError } = require('../../errors')
const { authentication, authorization } = require('../auth')
const jestHack = require('../../../test/helpers/jestHack')

jest.mock('paseto', () => ({
  V2: {
    sign: () => null,
    generateKey: () => null,
    verify: async () => Promise.resolve({ id: 1, type: 'managers' }),
  },
}))
jest.mock('crypto', () => ({ createPrivateKey: () => null }))
jest.mock('../../modules/managers/model', () => ({
  findOne: () => null,
  findById: () => null,
}))

describe('Auth middleware', function () {
  const req = { headers: { authorization: `Bearer TOKEN` }}
  const res = ({ status: jest.fn(() => ({ json: data => data })) })
  const next = jest.fn()


  afterAll(jestHack)


  it('Should authenticate correct login credentials', async function () {
    const middlewareCallback = authentication('managers')
    await middlewareCallback({
      ...req,
      body: { login: 'manager', password: '1' },
    }, res, next)

    expect(next).toBeCalled()
  })

  it('Should authorize allowed user types', async function () {
    const middlewareCallback = authorization(['users'])
    await middlewareCallback(req, res, next)

    expect(next).toBeCalled()
    expect(req).not.toHaveProperty('user')
  })

  it('Should authorize existenting user', async function () {
    const middlewareCallback = authorization(['managers'])
    await middlewareCallback(req, res, next)

    expect(next).toBeCalled()
    expect(req).not.toHaveProperty('user')
  })
})