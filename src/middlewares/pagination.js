const { get } = require('lodash')

module.exports = () => function pagination(req, res, next) {
  if (get(req, 'result.total') !== undefined) {
    const page = get(req, 'query.page') ? get(req, 'query.page') : 1
    const perPage = get(req, 'result.limit')
    const pages = Math.ceil(get(req, 'result.total') / perPage)

    return res.status(200).json({
      page,
      pages,
      total: get(req, 'result.total'),
      data: get(req, 'result.data'),
    })
  }

  next()
}
