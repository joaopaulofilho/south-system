// https://github.com/panva/paseto

const { createPrivateKey } = require('crypto')
const { compareSync } = require('bcryptjs')
const { V2: { sign, verify } } = require('paseto')
const { omit, get, set } = require('lodash')
const { Joi } = require('../core/validation')
const { UnauthorizedError, ValidationError } = require('../errors')

// eslint-disable-next-line
const pk = process.env.APP_PRIVATE_KEY.replace(/\\n/g, "\n")

function validation(req) {
  const schema = Joi.object({
    login: Joi.string().required(),
    password: Joi.string().required(),
  })

  const { error } = schema.validate(req.body)

  if (error) {
    throw new ValidationError('Invalid parameters', error)
  }
}

async function generateToken(id, type) {
  const privateKey = await createPrivateKey(pk)
  const accessToken = await sign({ id, type }, privateKey)
  return {
    privateKey,
    accessToken,
  }
}

function authentication(type = 'managers') {
  return async (req, res, next) => {
    validation(req)

    const login = get(req, 'body.login')
    const password = get(req, 'body.password')

    const Model = require(`../modules/${type}/model`)
    const register = await Model.findOne({ login })

    if (!register || !compareSync(password, register.password)) {
      return next(new UnauthorizedError())
    }

    const { accessToken } = await generateToken(register._id, type)

    res.status(200).json({
      accessToken,
      user: omit(register.toJSON(), [
        'createdAt',
        'updatedAt',
        'deletedAt',
        'password',
      ])
    })
  }
}

function authorization(allowUserType = ['manager']) {
  return async (req, res, next) => {
    const token = String(req.headers['authorization']).replace('Bearer ', '')

    if (!token) {
      // console.log('token not found')
      return next(new UnauthorizedError())
    }

    let register, typeUser

    try {
      const privateKey = await createPrivateKey(pk)
      const { id, type } = await verify(token, privateKey)

      if (allowUserType.indexOf(type) === -1) {
        // console.log('user type not allowed')
        return next(new UnauthorizedError())
      }

      const Model = require(`../modules/${type}/model`)

      if (!Model) {
        // console.log('user model not found', type, Model)
        return next(new UnauthorizedError())
      }

      register = await Model.findById(id)

      if (!register) {
        // console.log('user not found')
        return next(new UnauthorizedError())
      }

      typeUser = type
    } catch (e) {
      // console.log('unknown error', e)
      return next(new UnauthorizedError())
    }

    req.user = register
    set(req, 'user.type', typeUser)

    next()
  }
}

module.exports = {
  authentication,
  authorization,
  generateToken,
}
